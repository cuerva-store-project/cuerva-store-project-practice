package project.cuervastore.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CuervaStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuervaStoreApplication.class, args);
	}

}
