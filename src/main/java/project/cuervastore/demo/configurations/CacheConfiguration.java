package project.cuervastore.demo.configurations;

import java.time.Duration;

import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Ticker;

@Configuration
public class CacheConfiguration {

   @Bean
   @Primary
   public CacheManager cacheManager() {
      CaffeineCacheManager defaultManager = new CaffeineCacheManager();
      defaultManager.setCaffeine(Caffeine.newBuilder()
                                         .expireAfterWrite(Duration.ofSeconds(10))
                                         .maximumSize(50).ticker(Ticker.systemTicker()));
      defaultManager.setAllowNullValues(true);

      return defaultManager;
   }

}


