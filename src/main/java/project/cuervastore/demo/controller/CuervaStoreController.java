package project.cuervastore.demo.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import project.cuervastore.demo.service.CuervaStoreService;
import project.cuervastore.demo.service.dto.ProductRS;

@RestController
@RequestMapping
public class CuervaStoreController {

   private final CuervaStoreService cuervaStoreService;

   @Autowired
   public CuervaStoreController(CuervaStoreService cuervaStoreService) {
      this.cuervaStoreService = cuervaStoreService;
   }

   @GetMapping("/product/all")
   public ResponseEntity<List<ProductRS>> getAllProducts() {
      return cuervaStoreService.getAllProducts();
   }

   @GetMapping("/product/{id}")
   public ResponseEntity<ProductRS> getProductById(@PathVariable Integer id) {
      return cuervaStoreService.getProductById(id);
   }

   @PostMapping("product/modify/{id}")
   public ResponseEntity<ProductRS> modifyProductPrice(@PathVariable Integer id, @RequestParam String name, @RequestParam BigDecimal price,
         @RequestParam String type, @RequestParam int stock) {
      return cuervaStoreService.modifyProduct(id, name, price, type, stock);
   }

   @PostMapping("product/delete/{id}")
   public ResponseEntity deleteProduct(@PathVariable Integer id) {
      return cuervaStoreService.deleteProduct(id);
   }

}
