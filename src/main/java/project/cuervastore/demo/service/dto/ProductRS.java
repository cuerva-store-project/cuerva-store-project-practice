package project.cuervastore.demo.service.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductRS {

   private Integer id;

   private String name;

   private BigDecimal price;

   private String type;

   private int stock;

   private String error;

}
