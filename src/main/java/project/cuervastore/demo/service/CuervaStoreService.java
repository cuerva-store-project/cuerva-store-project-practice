package project.cuervastore.demo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import project.cuervastore.demo.repository.ProductRepository;
import project.cuervastore.demo.repository.models.Product;
import project.cuervastore.demo.service.dto.ProductRS;

@Service
public class CuervaStoreService {

   private final CacheManager cacheManager;

   private final ProductRepository productRepository;

   @Autowired
   public CuervaStoreService(CacheManager cacheManager, ProductRepository productRepository) {
      this.cacheManager = cacheManager;
      this.productRepository = productRepository;
   }

   @Cacheable
   public ResponseEntity<List<ProductRS>> getAllProducts() {
      List<ProductRS> productRSList = new ArrayList<>();
      if (productRepository.findAll().isEmpty()) {
         productRSList.add(setErrorProductRS());
         return new ResponseEntity<>(productRSList, HttpStatus.NOT_FOUND);
      } else {
         for (Product product : productRepository.findAll()) {
            productRSList.add(setProductRS(product));
         }
         return new ResponseEntity<>(productRSList, HttpStatus.OK);
      }
   }

   @Cacheable
   public ResponseEntity<ProductRS> getProductById(Integer id) {
      Optional<Product> optionalProduct = productRepository.findById(id);
      if (optionalProduct.isPresent()) {
         return new ResponseEntity<>(setProductRS(optionalProduct.get()), HttpStatus.OK);
      }
      return new ResponseEntity<>(setErrorProductRS(), HttpStatus.NOT_FOUND);
   }

   public ResponseEntity<ProductRS> modifyProduct(Integer id, String name, BigDecimal price, String type, int stock) {
      Optional<Product> optionalProduct = productRepository.findById(id);
      if (optionalProduct.isPresent()) {
         optionalProduct.get().setName(name);
         optionalProduct.get().setPrice(price);
         optionalProduct.get().setType(type);
         optionalProduct.get().setStock(stock);
         productRepository.save(optionalProduct.get());
         return new ResponseEntity<>(setProductRS(optionalProduct.get()), HttpStatus.OK);
      }
      return new ResponseEntity<>(setErrorProductRS(), HttpStatus.NOT_FOUND);
   }

   public ResponseEntity deleteProduct(Integer id) {
      Optional<Product> optionalProduct = productRepository.findById(id);
      if (optionalProduct.isPresent()) {
         productRepository.delete(optionalProduct.get());
         return new ResponseEntity<>(HttpStatus.OK);
      }
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
   }

   private ProductRS setProductRS(Product product) {
      ProductRS productRS = new ProductRS();
      productRS.setId(product.getId());
      productRS.setName(product.getName());
      productRS.setPrice(product.getPrice());
      productRS.setType(product.getType());
      productRS.setStock(product.getStock());
      return productRS;
   }

   private ProductRS setErrorProductRS() {
      ProductRS productRS = new ProductRS();
      productRS.setError("Prodcut not found");
      return productRS;
   }

}
