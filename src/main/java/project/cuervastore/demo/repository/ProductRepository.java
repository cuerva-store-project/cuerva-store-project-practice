package project.cuervastore.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import project.cuervastore.demo.repository.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}
